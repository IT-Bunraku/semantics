subdomains = {
    'meta':     dict(icon='diamond',        title="Meta"),
    'schema':   dict(icon='sitemap',        title="Schema"),
    'graph':    dict(icon='connectdevelop', title="Graph"),
    'linguist': dict(icon='flag',           title="Linguist"),
}

