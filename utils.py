from gestalt.web.utils       import *

from uchikoma.connector.utils       import *
from uchikoma.connector.models      import *
from uchikoma.connector.schemas     import *

################################################################################

# from django.db.models.base import ModelBase
# from django.db.models.query import QuerySet
# from django.db.models import Avg, Max, Min, Count, Q
# import operator
# 
# from fb_utils.utils import djfacetlog
# from djfacet.constants import *

# 
# from djfacet.cache_manager import *
# from djfacet.models import * 
# from djfacet.facet import *
# from djfacet.facetsgroup import *
# from djfacet.facetvalue import *
# from djfacet.queryhelper import *

from django.core.cache import cache

def clearExpiredSessions():
	"""
	Util that empties the expired Django sessions from the DB table.
	2012-09-04: added if statement based on tot num of rows in table
	2012-09-24: DISABLED cause it was causing errors
	"""
	# djfacetlog("---UTILS/ClearExpiredSessions(): starting")
	from django.contrib.sessions.models import Session
	tot = Session.objects.count()
	if tot > 50:
		djfacetlog("---UTILS/ClearExpiredSessions(): cleaning table with expired sessions...")
		import datetime
		Session.objects.filter(expire_date__lte = datetime.datetime.now()).delete()
		djfacetlog("...... clearExpiredSessions() applied succesfully :-) (threshold=50)")
	# else:
	# 	djfacetlog("...... not run as session table has only %d rows (threshold=50)" % tot)
################################################################################

DYNO_STATEs = (
    ('sleepy', "Sleepy & Inactive"),
    ('active', "Active & Alive"),
    ('stale',  "Inexistant & Deleted"),
)

DYNO_REPOs = (
    ('github',    "GitHub Inc"),
    ('bitbucket', "Atlassian BitBucket"),
    ('gitlab',    "GitLab Community"),
)

DYNO_PLATFORMs = (
    ('heroku',    "Heroku Dyno"),
    ('openshift', "OpenShift App"),
)

DYNO_BACKENDs = (
    ('mlab',       "mLab MongoDB"),
    ('graphenedb', "Graphene DB"),
    ('logentries', "LogEntries"),
    ('newrelic',   "NewRelic Insights"),
)
