#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

###################################################################################

@extend_reactor('sparql')
class Extension(ReactorExt):
    def request_raw(self, endpoint, query, **params):
        cnx = SparqlWrapper(endpoint)

        cnx.setQuery(query)

        cnx.setReturnFormat(SparqlJSON)

        return cnx.query().convert()

    #***************************************************************************

    def request_table(self, endpoint, query, **params):
        cols, rows = {}, []

        results = self.sparql_raw(endpoint, query, **params)

        for result in results["results"]["bindings"]:
            entry = {}

            for field in result.keys():
                cols[field] = field

                rows.append(result)

            print(result)

        return cols, rows

    #***************************************************************************

    def request_mime(self, endpoint, mimetype, query, **params):
        req = requests.get(endpoint, params={
            'default-graph-uri':    'http://dbpedia.org',
            'query':                query,
            'format':               mimetype,
            'timeout':              '30000',
            'debug':                'on',
            'CXML_redir_for_subjs': '121',
            'CXML_redir_for_hrefs': '',
        })

        return req.text

    #***************************************************************************

    def keyword(self, literal, roles=['NN']):
        qs = [
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>',
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
            'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
            'PREFIX skos: <http://www.w3.org/2004/02/skos/core#>',
            'PREFIX quepy: <http://www.machinalis.com/quepy#>',
            'PREFIX dbpedia: <http://dbpedia.org/ontology/>',
            'PREFIX dbpprop: <http://dbpedia.org/property/>',
            'PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>',
            'PREFIX type: <http://dbpedia.org/class/yago/>',
            'PREFIX prop: <http://dbpedia.org/property/>',
            '',
        ]

        qs += ['SELECT ?word, ?link, ?type WHERE {']

        qs += ['    ?link rdf:type ?type.']
        qs += ['    ?link rdfs:label "%s"@en.']
        qs += ['    ?link rdfs:label ?word.']

        qs += ['}']

        qs = '\n'.join(qs)

        print qs

        ds = self.sparql_raw()

        #ds['']        

