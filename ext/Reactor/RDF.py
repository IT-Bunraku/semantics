#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

import rdflib

###################################################################################

@extend_reactor('rdf')
class Sch_RDF(ReactorExt):
    def initialize(self, *args, **kwargs):
        pass

    #**********************************************************************************

    URIRef    = rdflib.URIRef
    BNode     = rdflib.BNode
    Literal   = rdflib.Literal
    NS        = rdflib.Namespace
    Graph     = rdflib.Graph

    Prefix    = rdflib.namespace

    #*******************************************************************************

    class Descriptor(object):
        # OWL DC
        # SKOS XSD DCTERMS
        # RDF FOAF DOAP
        mapping = dict([
            (key.lower(),hnd)
            for key,hnd in [
                (x, getattr(rdflib.namespace, x))
                for x in rdflib.namespace.__all__
            ]
            if type(hnd) is rdflib.Namespace
        ]+[
            #('rdf',  rdflib.URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns') ),
            #('foaf', rdflib.URIRef(u'http://xmlns.com/foaf/0.1/')                 ),
        ])

        def prefix(self, ns, *path):
            if ns in self.mapping:
                resp = self.mapping[ns]

                for part in path:
                    if hasattr(resp, part):
                        resp = getattr(resp, part)
                    else:
                        return None

                return resp

            return None

        def literal(self, value):
            resp = value

            if type(resp) in (bool, int, long, float, str, unicode):
                resp = rdflib.Literal(resp)
            elif type(resp) in (tuple, set, frozenset, float, str, unicode):
                pass
            elif type(resp) in (dict,):
                pass

            return resp

        def populate(self, entry, graph=None):
            if graph is None:
                graph = rdflib.Graph()

            sbj = self.retrieve(entry.__dict__)

            for s,p,o in self.describe(entry, sbj):
                g.add( (s,p,o) )

            return graph

        def export(self, entry):
            grp = self.populate(entry)

            resp = grp.serialize(format='turtle')

            return resp

