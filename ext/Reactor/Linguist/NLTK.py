#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

from .abstract import *

###################################################################################

@extend_reactor('nltk')
class Extension(NLP_Extension):
    def ensure(self):
        if len([x for x in os.listdir(os.environ['NLTK_DATA']) if x!='.gitkeep'])==0:
            import nltk

            for corp in os.environ.get('NLTK_CORPUS', 'book').split(' '):
                nltk.download(corp)

            #call_command('setup', '--nltk')

    def parse(self, *args, **kwargs):
        return self.task('nlp_sentence', *args, **kwargs)

    def process(self, *args, **kwargs):
        return self.task('nlp_sentence', *args, **kwargs)

    #***************************************************************************

    def flatten(self, item):
        lst,obj = [], item

        while len(obj)==2:
            obj, val = obj[0], obj[1]

            lst += [val]

        return obj, lst

