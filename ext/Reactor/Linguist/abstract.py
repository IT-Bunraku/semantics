#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

import nltk, quepy

###################################################################################

class NLP_Extension(ReactorExt):
    def task(self, handler, *args, **kwargs):
        if handler in [Linguist.nlp_sentence]:
            return handler(*args, **kwargs)
        else:
            cb = handler.delay(*args, **kwargs)

            return cb.wait()

