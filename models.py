#-*- coding: utf-8 -*-

from .utils import *

###################################################################################################

DATA_PARTICLEs = (
    ('model',  "Django Model"),

    ('schema', "Mongo Document"),
    ('embed',  "Mongo Embedded"),

    ('node',   "Neo4j Node"),
    ('edge',   "Neo4j Edge"),
)

class DataSchema(models.Model):
    alias      = models.CharField(max_length=48)
    title      = models.CharField(max_length=128, blank=True)

    app_name   = models.CharField(max_length=48)
    particle   = models.CharField(max_length=24, choices=DATA_PARTICLEs)

    def handler(self):
        resp = None

        if self.particle=='model':
            pass

        elif self.particle=='schema':
            pass
        elif self.particle=='embed':
            pass

        elif self.particle=='node':
            pass
        elif self.particle=='edge':
            pass

        return resp

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "Data schema"
        verbose_name_plural = "Data schemas"

#*******************************************************************************

class DataPredicate(models.Model):
    schema     = models.ForeignKey(DataSchema, related_name='predicates')
    alias      = models.CharField(max_length=128)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "Data predicate"
        verbose_name_plural = "Data predicates"

################################################################################

class RdfNamespace(models.Model):
    alias = models.CharField(max_length=64)
    link  = models.CharField(max_length=256)

    prefix = property(lambda self: 'PREFIX %(alias)s: <%(link)s>' % self.__dict__)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "RDF namespace"
        verbose_name_plural = "RDF namespaces"

#*******************************************************************************

class OwlOntology(models.Model):
    namespace = models.ForeignKey(RdfNamespace, related_name='ontologies')
    alias     = models.CharField(max_length=64)
    link      = models.CharField(max_length=256)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "OWL ontology"
        verbose_name_plural = "OWL ontologies"

##*******************************************************************************
#
#class RdfPredicate(models.Model):
#    ontology = models.ForeignKey(RdfOntology, related_name='predicates')
#    alias    = models.CharField(max_length=64)
#    xsd_type = models.CharField(max_length=128)

##################################################################################################

from SPARQLWrapper import SPARQLWrapper, JSON

class SparqlEndpoint(models.Model):
    alias      = models.CharField(max_length=64)
    link       = models.CharField(max_length=256)

    namespaces = models.ManyToManyField(RdfNamespace, related_name='endpoints')

    all_ns     = property(lambda self: self.namespaces.all())

    ns_listing = property(lambda self: [ns.alias for ns in self.all_ns])
    prefixes   = property(lambda self: '\n'.join([
        ns.prefix
        for ns in self.all_ns
    ]))

    #***************************************************************************

    def query(self, statement, *args, **kwargs):
        cmd = SPARQLWrapper(self.link)

        try:
            statement = statement % args
        except:
            try:
                statement = statement % kwargs
            except:
                pass

        statement = self.prefixes + '\n' + statement

        cmd.setQuery(statement)

        cmd.setReturnFormat(JSON)

        return cmd

    def execute(self, statement, *args, **kwargs):
        cmd = self.query(statement, *args, **kwargs)

        try:
            return cmd.query().convert()
        except:
            return None

    #***************************************************************************

    class Wrapper(DataSet):
        COLLECTION = 'sparql_ontology'

        def initialize(self):
            self.persist()

        def persist(self):
            cfg = dict(endpoint=self.link, narrow=self.narrow, ontology=self.data['class'], ancestor=self.data['parent'])

            doc = None

            try:
                doc = self.tribe.insert_one(cfg)
            except:
                pass

            #doc.save()

            return doc

    #***************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "SPARQL endpoint"
        verbose_name_plural = "SPARQL endpoints"

#*******************************************************************************

class SparqlQuery(models.Model):
    endpoint   = models.ForeignKey(SparqlEndpoint, related_name='queries')
    alias      = models.CharField(max_length=128)

    statement  = models.TextField()
    namespaces = models.ManyToManyField(RdfNamespace, related_name='queries')

    all_ns     = property(lambda self: dict([
        (ns.alias, ns)
        for qs in (self.endpoint.namespaces, self.namespaces)
        for ns in qs.all()
    ]).values())

    ns_listing = property(lambda self: [ns.alias for ns in self.all_ns])
    prefixes   = property(lambda self: '\n'.join([
        ns.prefix
        for ns in self.all_ns
    ]))

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "SPARQL query"
        verbose_name_plural = "SPARQL queries"

