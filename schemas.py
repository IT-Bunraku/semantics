from .utils import *

################################################################################

@Reactor.mongo.register_embed('comment')
class Comment(MongoEmbedded):
    created_at = MongoProp.DateTimeField(
        default=datetime.now, required=True, editable=False,
    )
    author = MongoProp.StringField(verbose_name="Name", max_length=255, required=True)
    email  = MongoProp.EmailField(verbose_name="Email")
    body = MongoProp.StringField(verbose_name="Comment", required=True)

    class RDF: #(Reactor.rdf.Descriptor):
        namespace = ''

        def create(self, narrow):
            resp = RDF_BNode() # a GUID is generated

            return resp

        def retrieve(self, narrow):
            resp = RDF_URIRef("http://example.org/people/Bob")

            return resp

        def describe(self, entry, subject):
            yield subject, self.prefix('rdf','type'), self.prefix('foaf','Person')

            yield subject, self.prefix('foaf','name'), self.literal("Bob")
            yield subject, self.prefix('foaf','name'), self.literal(24)

            #yield subject, self.prefix('foaf','knows'), self.literal(entry[])

#*******************************************************************************

@Reactor.mongo.register_schema('post')
class Post(MongoDocument):
    when = MongoProp.DateTimeField(
        default=datetime.now, required=True, editable=False,
    )
    title    = MongoProp.StringField(max_length=255, required=True)
    content  = MongoProp.StringField(max_length=255, required=True, primary_key=True)
    comments = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'))

