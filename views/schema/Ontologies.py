#!/usr/bin/env python
# encoding: utf-8

"""


##################
# 
# Semantic Vocabulary Browser
#
##################


The app assumes that the local vocabularies are stored in the "ontoview/static/models/" folder. 

Local files are shown only if the user is logged in as ADMIN.


"""


from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.html import strip_tags, escape
from django.contrib import messages

from StringIO import StringIO
import urllib2

from uchikoma.semantics.models import *
# from ontoview.templatetags.ontotags import truncchar_inverse

import os
	
from gestalt.web.helpers import Reactor

from django.conf import settings

# ps: this uses the local installation
from ontospy import *

from uchikoma.semantics.color_gradation import *
from django.core.cache import cache



LOCAL_ONTOLOGIES_FOLDER = Reactor.FILES('assets', 'ontoview', 'models')




def get_current_ontology(request, url, startpage=False):
	"""
	If testing in local, loads the ontology file from the local folder.
	Otherwise it just expects a standard rdf-returning URI.
	"""	   
	
	if url.startswith("file://localhost/"):

		# then it's a local file
		realpath = os.path.join(LOCAL_ONTOLOGIES_FOLDER, url.replace("file://localhost/", ""))	 
		# onto = getCached_Onto(request, realpath) 
		onto = Ontology(realpath)

		# hide the physical location set by OntoSpy (so to hide server path infos in display & url parameters)
		onto.ontologyMaskedLocation = url

		# override physical location set by OntosPy (so to allow source download via Django static handler)
		prefix = 'https://' if request.is_secure() else 'http://'
		if settings.STATIC_URL.startswith("http"):
			onto.ontologyPhysicalLocation = settings.STATIC_URL + 'ontoview/models/' + url.replace("file://localhost/", "")
		else:
			onto.ontologyPhysicalLocation = prefix + request.get_host() + settings.STATIC_URL + 'ontoview/models/' + url.replace("file://localhost/", "")
		return onto
	
	else: 
	
		if url.startswith("http://"):
			pass
		else:
			url = "http://" + url
	
		onto = getCached_Onto(request, url) # onto = Ontology(url)
		onto.ontologyMaskedLocation = None # no need to mask stuff here
	
		# in theory the onto has loaded succesfully - so we save it (ps: only if the request comes from the startpage!)
		if startpage:
			updateHistory(onto)
	
		# print str(onto)
		return onto
			




def getCached_Onto(request, ontoInstanceURI):
	""" 
	Uses the session/cache backend to avoid reloading an ontology every time
	
	Note that each time we're loading the ontology using an http request - and passing the file directly to rdflib.parse()
	"""
	ONTOVIEW_CACHE = cache.get(ontoInstanceURI)
		
	if not ONTOVIEW_CACHE:		
		printDebug("**NO CACHE** Could not find a cached version of %s..... now retrieving and caching...." % ontoInstanceURI)
		
		# get Source file 
		req = urllib2.Request(ontoInstanceURI)
		req.add_header('Accept', 'application/rdf+xml,text/rdf+n3;q=0.9,application/xhtml+xml;q=0.5, */*;q=0.1')
		
		
		res = urllib2.urlopen(req)
			
		ONTOVIEW_CACHE = res.read()
		cache.set(ontoInstanceURI, ONTOVIEW_CACHE, 300)	 # note: in seconds		
		onto = Ontology(StringIO(ONTOVIEW_CACHE))
		
		
		res.close()
		
	else:
		printDebug("**YES CACHE** Found a cached version of <%s>! " % ontoInstanceURI)
		onto = Ontology(StringIO(ONTOVIEW_CACHE))
	
	return onto








##################
#  
#  ONTO VIEWS
#
##################





def startsearch(request):
	""" 
	Home page that shows search box and history 
	
	If the user is logged in as ADMIN the local files are shown
	
	"""
	
	if request.user.is_authenticated():
		# show local files only to admin user
		local_ontologies = get_files()	# local files don't get added to history panel!
	else:
		local_ontologies = []
	
	history = HistoryEntry.objects.all().order_by('-pubdate')
		
	context = {	  
				'local_ontologies' : local_ontologies ,		
				'history' : history ,		
				}
	return render_to_response('ontoview/pages-site/startsearch.html', 
								context,
								context_instance=RequestContext(request))
								
								



def ontology(request):
	""" 
	View that shows the main metadata and stats about the ontology as a whole
	"""
	try:
		onto = get_current_ontology(request, request.GET.get('model', ''), request.GET.get('startpage', ''))
	except:# catch all errors while trying to load the ontology
		_message = "Sorry! The file you tried to load does not look like a valid RDF/OWL ontology."		
		messages.error(request, _message)
		return startsearch(request)
		
	
	alltriples = onto.ontologyAnnotations(niceURI=True, excludeProps=False, excludeBNodes = False,)
			
	context = {	  
				'stats' : onto.ontologyStats() ,
				'toplayer' : [onto.classRepresentation(aClass) for aClass in onto.toplayer] ,
				'sourcecode1' : onto.serializeOntologyGraph("turtle").strip() ,
				'sourcecode2' : onto.serializeOntologyGraph("xml").strip() ,				
				'alltriples' : alltriples,				
				}
	defaul_context = getDefaultContext(onto)
	context.update(defaul_context)
	return render_to_response('ontoview/pages-onto/ontology.html', 
								context,
								context_instance=RequestContext(request))



	
def classes(request):
	""" 
	View that shows the class tree, or specific class info
	"""

	resource = decodeuri(request.GET.get('resource', ''))				
	try:
		onto = get_current_ontology(request, request.GET.get('model', ''))
	except:# catch all errors while trying to load the ontology
		_message = "Sorry! The file you tried to load does not look like a valid RDF/OWL ontology."		
		messages.error(request, _message)
		return startsearch(request)	
	
	if resource:
		if onto.classFind(resource):
			# CLASS INFO
			aClass = onto.classFind(resource)[0]
			supers = onto.classAllSupers(aClass)
			alltree = supers + [aClass]
			subs = onto.classDirectSubs(aClass, sortUriName = True)
			# siblings = onto.classSiblings(aClass, sortUriName = True)			 
			domain_info = onto.classDomainFor(aClass, inherited = True)
			
			test = onto.classDomainFor(aClass, inherited = True)
			
			# for x in test:
			#	print x, len(x)
			
			alltriples = onto.entityTriples(aClass, niceURI=True, excludeProps=[RDF.type, RDFS.subClassOf, RDFS.isDefinedBy], excludeBNodes = False,) 
			
			alltree_diagram = []
			colors = interpolate("#FFFF00", "#E62914", len(alltree))  # #E62914 [red] or #FFFF00 [yellow]
			indexes_reversed = range(len(alltree))
			for x in indexes_reversed:	# iterate from last item
				rel_size = 70
				size = 100 + (rel_size * (len(alltree) - (x)))
				marginleft = (rel_size / 10) * 9
				margintop = (rel_size / 5) * 3
				alltree_diagram.append({'el' : onto.classRepresentation(alltree[x]), 'color' : colors[x], 'size' : size, 
					'marginleft' : marginleft, 'margintop' : margintop })										
			
			# explode the domain info nested list so to include all prop/class representation
			allDomainProperties = []
			for tupl in domain_info:
				classExploded = onto.classRepresentation(tupl[0])
				propExploded = [onto.propertyRepresentation(p) for p in tupl[1]]
				allDomainProperties.append((classExploded, propExploded))
			
			context = {	  
						'class' : onto.classRepresentation(aClass) ,
						'alltree' : [onto.classRepresentation(x) for x in alltree] ,
						'alltree_diagram' : alltree_diagram ,
						'subs' : [onto.classRepresentation(x) for x in subs] ,
						# 'siblings' : [onto.classRepresentation(x) for x in siblings] ,
						'allDomainProperties' : allDomainProperties ,
						'instances' : onto.classInstances(aClass) ,
						'alltriples' : alltriples,
						}
			defaul_context = getDefaultContext(onto)
			context.update(defaul_context)
		else:
			# ERROR MSG
			context = {	  
						'resource404' : True ,
						'resource' : resource ,
						}
			defaul_context = getDefaultContext(onto)
			context.update(defaul_context)
	else:	
		# CLASS TREE
		context = {	  
					'classtree' : formatHTML_ClassTree(onto) ,
					'classtreeTable' : formatHTML_ClassTreeTable(onto) ,
					}
		defaul_context = getDefaultContext(onto)
		context.update(defaul_context)
		
	return render_to_response('ontoview/pages-onto/classes.html', 
								context,
								context_instance=RequestContext(request))




def properties(request):
	""" 
	
	"""
	resource = decodeuri(request.GET.get('resource', ''))		
	try:
		onto = get_current_ontology(request, request.GET.get('model', ''))
	except:# catch all errors while trying to load the ontology
		_message = "Sorry! The file you tried to load does not look like a valid RDF/OWL ontology."		
		messages.error(request, _message)
		return startsearch(request)	
	
	if resource:
		if onto.propertyFind(resource):
			# PROPERTY INFO
			aProp = onto.propertyFind(resource)[0]
			supers = onto.propertyAllSupers(aProp)
			alltree = supers + [aProp]
			subs = onto.propertyDirectSubs(aProp, sortUriName = True)

			alltriples = onto.entityTriples(aProp, niceURI=True, excludeProps=[RDF.type, RDFS.subPropertyOf, RDFS.isDefinedBy, RDFS.domain, RDFS.range], excludeBNodes = False,) 
			
			alltree_diagram = []
			colors = interpolate("#ADD8E6", "#008000", len(alltree))  # #008000 [green] or #ADD8E6 [lightblue]
			indexes_reversed = range(len(alltree))
			for x in indexes_reversed:	# iterate from last item
				rel_size = 70
				size = 100 + (rel_size * (len(alltree) - (x)))
				marginleft = (rel_size / 10) * 9
				margintop = (rel_size / 5) * 3
				alltree_diagram.append({'el' : onto.classRepresentation(alltree[x]), 'color' : colors[x], 'size' : size, 
					'marginleft' : marginleft, 'margintop' : margintop })
		
			# props = [onto.classProperties(aClass)]
			context = {	  
						'property' : onto.propertyRepresentation(aProp) ,
						'alltree' : [onto.propertyRepresentation(x) for x in alltree] ,
						'alltree_diagram' : alltree_diagram ,
						'subs' : [onto.propertyRepresentation(x) for x in subs] ,
						'alltriples' : alltriples,
						}
			defaul_context = getDefaultContext(onto)
			context.update(defaul_context)
		else:
			# ERROR MSG
			context = {	  
						'resource404' : True ,
						'resource' : resource ,
						}
			defaul_context = getDefaultContext(onto)
			context.update(defaul_context)
						
	else:
		#PROPERTY TREE	
		context = {	  
					# 'datapropertiesTree' : formatHTML_PropTree(onto, classPredicate="owl.dataprop") ,
					'datapropertiesTree' : formatHTML_PropTreeTable(onto, classPredicate="owl.dataprop") ,
					# 'objpropertiesTree' : formatHTML_PropTree(onto, classPredicate="owl.objprop") ,
					'objpropertiesTree' : formatHTML_PropTreeTable(onto, classPredicate="owl.objprop") ,
					}
		defaul_context = getDefaultContext(onto)
		context.update(defaul_context)
		
	return render_to_response('ontoview/pages-onto/properties.html', 
								context,
								context_instance=RequestContext(request))





def individuals(request):
	""" 
	
	"""
	resource = decodeuri(request.GET.get('resource', ''))	
	try:
		onto = get_current_ontology(request, request.GET.get('model', ''))
	except:# catch all errors while trying to load the ontology
		_message = "Sorry! The file you tried to load does not look like a valid RDF/OWL ontology."		
		messages.error(request, _message)
		return startsearch(request)	

	if resource:
		if onto.instanceFind(resource):
			instance = onto.instanceFind(resource)[0]
			instance_repr = onto.instanceRepresentation(instance)
			siblings = onto.instanceSiblings(instance)
			# triples = onto.entityTriples(instance, excludeProps=[RDF.type, RDFS.comment])
			# INDIVIDUAL INFO
			context = {	  
						'instance' : instance_repr ,
						'siblings' : siblings ,
						'triples' : [(onto.propertyRepresentation(p), o) for p,o in instance_repr['alltriples']] ,
						}
			defaul_context = getDefaultContext(onto)
			context.update(defaul_context)
		else:
			# ERROR MSG
			context = {	  
						'resource404' : True ,
						'resource' : resource ,
						}
			defaul_context = getDefaultContext(onto)
			context.update(defaul_context)
						
	else:
		# ALL INDIVIDUALS	
		
		context = {	  
					'instances' : [onto.instanceRepresentation(instance) for instance in onto.allinstances] ,
					}
		defaul_context = getDefaultContext(onto)
		context.update(defaul_context)
		
	return render_to_response('ontoview/pages-onto/individuals.html', 
								context,
								context_instance=RequestContext(request))









##################
#  
#  TREE DISPLAY FUNCTIONS
#
##################



def formatHTML_ClassTreeTable(onto, treedict = None, element = 0):
	""" outputs an html tree representation based on the dictionary we get from the Inspector
	object....

	EG: 
	<table class=h>
	
		<tr>
		  <td class="tc" colspan=4><a href="../DataType">DataType</a>
		  </td>
		</tr>
		<tr>
		  <td class="tc" colspan=4><a href="../DataType">DataType</a>
		  </td>
		</tr>
	
		<tr>
		  <td class="space"></td>
		  <td class="bar"></td>
		  <td class="space"></td>

		  <td>
			<table class=h>
			   <tr><td class="tc" colspan=4><a href="../Boolean">Boolean</a>
					</td>
			   </tr>
			   <tr><td class="tc" colspan=4><a href="../Boolean">Boolean</a>
					</td>
			   </tr>
		   </table>
		  </td>
	  
	  
		 </tr>
	 </table>

	
	Note: The top level owl:Thing never appears as a link.
	
	"""
	ontoFile = onto.ontologyMaskedLocation or onto.ontologyPhysicalLocation
	if not treedict:
		treedict = onto.ontologyClassTree
	stringa = """<table class="h">"""
	for x in treedict[element]:
		if onto.uri2niceString(x) == "owl:Thing":
			stringa += """<tr>
							<td class="tc" colspan=4><a href="#">%s</a></td>
						  </tr>""" % (truncchar_inverse(onto.uri2niceString(x), 50))			  
		else:
			stringa += """<tr>
							<td class="tc" colspan=4><a title=\"%s\" class=\"treelinks\" href=\"?model=%s&resource=%s\">%s</a></td>
						  </tr>""" % (str(x), ontoFile, encodeuri(x) , truncchar_inverse(onto.uri2niceString(x), 50))
			
		if treedict.get(x, None):
			stringa += """ <tr>
							<td class="space"></td>
							<td class="bar"></td>
							<td class="space"></td>
							<td>%s</td>
							</tr>""" % formatHTML_ClassTreeTable(onto, treedict, x)
					  
		# stringa += formatHTML_ClassTree(onto, treedict, x)
		# stringa += "</li>"
	stringa += "</table>"
	return stringa
	
	
	




def formatHTML_PropTreeTable(onto, classPredicate, treedict = None, element = 0):
	""" outputs an html tree representation based on the dictionary we get from the Inspector
	object....
	-see above for an example-
	
	if not treedict:
		if classPredicate == "owl.objprop":
			treedict = onto.ontologyObjPropertyTree
		else:
			treedict = onto.ontologyDataPropertyTree
	stringa = "<ul>"
	for x in treedict[element]:
		stringa += "<li><a title=\"%s\" class=\"treelinks propcolor\" href=\"?model=%s&resource=%s\">%s</a>" % (str(x), 
			ontoFile, encodeuri(x) , onto.uri2niceString(x))
		stringa += formatHTML_PropTree(onto, classPredicate, treedict, x)
		stringa += "</li>"
	stringa += "</ul>"
	return stringa
	
	
	
	"""
	ontoFile = onto.ontologyMaskedLocation or onto.ontologyPhysicalLocation
	if not treedict:
		if classPredicate == "owl.objprop":
			treedict = onto.ontologyObjPropertyTree
		else:
			treedict = onto.ontologyDataPropertyTree

	stringa = """<table class="h propcolor">"""
	for x in treedict[element]:
		stringa += """<tr>
						<td class="tc" colspan=4><a title=\"%s\" class=\"treelinks\" href=\"?model=%s&resource=%s\">%s</a></td>
					  </tr>""" % (str(x), ontoFile, encodeuri(x) , truncchar_inverse(onto.uri2niceString(x), 50))
			
		if treedict.get(x, None):
			stringa += """ <tr>
							<td class="space"></td>
							<td class="bar"></td>
							<td class="space"></td>
							<td>%s</td>
							</tr>""" % formatHTML_PropTreeTable(onto, classPredicate, treedict, x)
					  
		# stringa += formatHTML_ClassTree(onto, treedict, x)
		# stringa += "</li>"
	stringa += "</table>"
	return stringa
	
	
	
	


#
#
# ===========
# UTILS
# ===========
#
#





def get_files():
	mypath = LOCAL_ONTOLOGIES_FOLDER
	onlyfiles = [ "file://localhost/" + f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath,f)) and not f.startswith(".") ]
	return onlyfiles


def bootstrapDesc(onto):
	"""
	Extract whatever could be used as a description for the ontology
	"""
	DCTERMS = Namespace('http://purl.org/dc/terms/')		
	DC = Namespace('http://purl.org/dc/elements/1.1/')

	RDFSlabel = "\n".join([x for x in onto.rdfGraph.objects(onto.ontologyURI(), RDFS.label)])
	RDFScomment = "\n".join([x for x in onto.rdfGraph.objects(onto.ontologyURI(), RDFS.comment)])

	DCdescription = "\n".join([x for x in onto.rdfGraph.objects(onto.ontologyURI(), DC.description)])
	DCtitle = "\n".join([x for x in onto.rdfGraph.objects(onto.ontologyURI(), DC.description)])
	DCTERMSdescription = "\n".join([x for x in onto.rdfGraph.objects(onto.ontologyURI(), DCTERMS.description)])
	DCTERMStitle = "\n".join([x for x in onto.rdfGraph.objects(onto.ontologyURI(), DCTERMS.description)])
	
	return	" ".join([DCtitle, DCdescription, DCTERMStitle, DCTERMSdescription, RDFSlabel, RDFScomment])	


def updateHistory(onto):
	""" update the history table once a URI is succesfully loaded into an ontology """
	try:
		# if the URI already exists, just save so to increase the count
		h = HistoryEntry.objects.get(uri=onto.ontologyPhysicalLocation.strip())
		h.save()
	except:
		# create a new history entry
		h = HistoryEntry(uri=onto.ontologyPhysicalLocation.strip(), description=bootstrapDesc(onto))
		h.save()	



def encodeuri(u):
	"""
	hashes are interpreted differently in urls and ontology uris, so we mask them when passed as args 
	"""
	return u.replace("#", "*hash*")	 # &#x23; in html TODO
def decodeuri(u):
	""" 
	"""
	return u.replace("*hash*", "#")	 


# note: duplicate of templatetagg so to avoid circular imports
def truncchar_inverse(value, arg):
	if len(value) < arg:
		return value
	else:
		x = len(value) - arg
		return '...' + value[x:] 
		

def getDefaultContext(onto):
	"""
	Refactoring stuff that we always want in context
	"""
		
	context = {	  
				'ontoFile' : onto.ontologyMaskedLocation or onto.ontologyPhysicalLocation,
				'ontoFileNoMask' : onto.ontologyPhysicalLocation ,
				'ontoPrettyUri' : onto.ontologyPrettyURI ,
				'namespaces' : onto.ontologyNamespaces() ,
				
				'TOTclasses' : len(onto.allclasses) ,
				'TOTproperties' : len(onto.allproperties) ,
				'TOTindividuals' : len(onto.allinstances) ,
				}				
				
	return context


def printDebug(s):
	try:
		print s
	except: 
		pass




















	
	
	
#	
#	
# ============= 
# old tree formatting algorithms: work but deprecated 
# ===========	
# 
# 

	

def formatHTML_ClassTree(onto, treedict = None, element = 0):
	""" outputs an html tree representation based on the dictionary we get from the Inspector
	object....

	EG: 
	<ul id="example" class="filetree">
		<li><a class="folder">Folder 2</a>
			<ul>
				<li><a class="folder">Subfolder 2.1</a>
					<ul>
						<li><a class="file">File 2.1.1</a></li>
						<li><a class="file">File 2.1.2</a></li>
					</ul>
				</li>
				<li><a class="file">File 2.2</a></li>
			</ul>
		</li>
		<li class="closed"><a class="folder">Folder 3 (closed at start)</a></li>
		<li><a class="file">File 4</a></li>
	</ul>
	
	Note: The top level owl:Thing never appears as a link.
	
	"""
	ontoFile = onto.ontologyMaskedLocation or onto.ontologyPhysicalLocation
	if not treedict:
		treedict = onto.ontologyClassTree
	stringa = "<ul>"
	for x in treedict[element]:
		if onto.uri2niceString(x) == "owl:Thing":
			stringa += """<li>%s""" % (onto.uri2niceString(x))			  
		else:
			stringa += """<li><a title=\"%s\" class=\"treelinks\" href=\"?model=%s&resource=%s\">%s</a>""" % (str(x), 
			ontoFile, encodeuri(x) , onto.uri2niceString(x))
		stringa += formatHTML_ClassTree(onto, treedict, x)
		stringa += "</li>"
	stringa += "</ul>"
	return stringa



	

def formatHTML_PropTree(onto, classPredicate, treedict = None, element = 0):
	""" outputs an html tree representation based on the dictionary we get from the Inspector
	object....	
	"""
	ontoFile = onto.ontologyMaskedLocation or onto.ontologyPhysicalLocation
	if not treedict:
		if classPredicate == "owl.objprop":
			treedict = onto.ontologyObjPropertyTree
		else:
			treedict = onto.ontologyDataPropertyTree
	stringa = "<ul>"
	for x in treedict[element]:
		stringa += "<li><a title=\"%s\" class=\"treelinks propcolor\" href=\"?model=%s&resource=%s\">%s</a>" % (str(x), 
			ontoFile, encodeuri(x) , onto.uri2niceString(x))
		stringa += formatHTML_PropTree(onto, classPredicate, treedict, x)
		stringa += "</li>"
	stringa += "</ul>"
	return stringa

