#-*- coding: utf-8 -*-

from uchikoma.semantics.shortcuts import *

################################################################################

#@Reactor.router.register_route('schema', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "schema/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

