from uchikoma.semantics.shortcuts import *

################################################################################

@login_required
@render_to('graph/views/explorer.html')
def neo_graph(request, narrow='default'):
    return context(partition=narrow)

@login_required
def neo_payload(request, narrow):
    mapping = {
        'people': dict(
            query=dict(
                source=['Person'],
                target=['Brand','Company'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                Brand=(lambda node,prop: prop['title']),
            ),
            label=['name','full'],
        ),
        'systems': dict(
            query=dict(
                source=['OS','Appliance'],
                target=['Application','EmbeddedSystem','Platform'],
            ),
            custom=dict(
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
        'skills': dict(
            query=dict(
                source=['Person', 'Language','Framework'],
                target=['Platform','Application'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
    }

    if narrow in mapping:
        lookup = "MATCH s-[r]-t WHERE ("
        lookup += "s.ontology in ['%s']" % "','".join(mapping[narrow]['query']['source'])
        lookup += ") AND ("
        lookup += "t.ontology in ['%s']" % "','".join(mapping[narrow]['query']['target'])
        lookup += ") RETURN s,r"

        nodes,edges = view_cypher(VORTEX_DEVOPS, lookup, mapping[narrow]['label'], mapping[narrow]['custom'])
        
        nodes,edges = [
    {"size": 60, "score": 0, "id": "Androsynth", "type": "circle"},
    {"size": 10, "score": 0.2, "id": "Chenjesu", "type": "circle"},
    {"size": 60, "score": 0.4, "id": "Ilwrath", "type": "circle"},
    {"size": 10, "score": 0.6, "id": "Mycon", "type": "circle"},
    {"size": 60, "score": 0.8, "id": "Spathi", "type": "circle"},
    {"size": 10, "score": 1, "id": "Umgah", "type": "circle"},
    {"id": "VUX", "type": "circle"},
    {"size": 60, "score": 0, "id": "Guardian", "type": "square"},
    {"size": 10, "score": 0.2, "id": "Broodhmome", "type": "square"},
    {"size": 60, "score": 0.4, "id": "Avenger", "type": "square"},
    {"size": 10, "score": 0.6, "id": "Podship", "type": "square"},
    {"size": 60, "score": 0.8, "id": "Eluder", "type": "square"},
    {"size": 10, "score": 1, "id": "Drone", "type": "square"},
    {"id": "Intruder", "type": "square"}
        ],[
    {"source": 0, "target": 1},
    {"source": 0, "target": 2},
    {"source": 0, "target": 3},
    {"source": 0, "target": 4},
    {"source": 0, "target": 5},
    {"source": 0, "target": 6},
    {"source": 1, "target": 3},
    {"source": 1, "target": 4},
    {"source": 1, "target": 5},
    {"source": 1, "target": 6},
    {"source": 2, "target": 4},
    {"source": 2, "target": 5},
    {"source": 2, "target": 6},
    {"source": 3, "target": 5},
    {"source": 3, "target": 6},
    {"source": 5, "target": 6},
    {"source": 0, "target": 7},
    {"source": 1, "target": 8},
    {"source": 2, "target": 9},
    {"source": 3, "target": 10},
    {"source": 4, "target": 11},
    {"source": 5, "target": 12},
    {"source": 6, "target": 13}
        ]

        return JsonResponse({
            "graph": [],
            "directed":   False,
            "multigraph": False,
            
            "links": nodes,
            "nodes": edges,
        })
    else:
        raise Http404()

