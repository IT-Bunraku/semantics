#-*- coding: utf-8 -*-

from uchikoma.semantics.shortcuts import *

################################################################################

@Reactor.router.register_route('graph', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "graph/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

@Reactor.router.register_route('graph', r'^search$', strategy='login')
class Search_Box(Reactor.ui.Tool):
    form_class    = SearchForm
    template_name = 'graph/views/search.html'

    def process(self, request, context, data):
        #context['results'] = self.nlp_parse('en', data['sentences'])

        return context

################################################################################

@Reactor.router.register_route('graph', r'^sparql$', strategy='login')
class SparQL_CMD(Reactor.ui.Tool):
    form_class    = SparqlForm
    template_name = 'graph/views/sparql.html'

    def process(self, request, context, data):
        #context['results'] = self.nlp_parse('en', data['sentences'])

        return context

################################################################################

from . import Explorer
