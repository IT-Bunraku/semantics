#-*- coding: utf-8 -*-

from uchikoma.semantics.shortcuts import *

################################################################################

@Reactor.router.register_route('linguist', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "linguist/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

@Reactor.router.register_route('linguist', r'^tokenize$', strategy='login')
class Tokenize(Reactor.ui.Tool):
    form_class    = TokenizeForm
    template_name = 'linguist/views/tokenizer.html'

    def process(self, request, context, data):
        context['results'] = Reactor.nlp.parse('en', data['sentences'])

        return context

################################################################################

@Reactor.router.register_route('linguist', r'^understand$', strategy='login')
class Understand(Reactor.ui.Tool):
    form_class    = QuestionForm
    template_name = 'linguist/views/understand.html'

    def process(self, request, context, data):
        vortex = quepy.install("gestalt.que.dbpedia")

        results = {}

        results['target'], results['query'], results['meta'] = vortex.get_query(data['statement'])

        results['query'] = results['query'].replace('\n', '\n\n')

        while results['query'].startswith('\n'):
            results['query'] = results['query'][1:]

        results['cols'], results['rows'] = Reactor.sparql.table('http://dbpedia.org/sparql', results['query'])

        context['results'] = results

        return context

#*******************************************************************************

@Reactor.router.register_route('linguist', r'^interpret$', strategy='login')
class Interpret(Reactor.ui.Tool):
    form_class    = InterpretForm
    template_name = 'linguist/views/interpret.html'

    def process(self, request, context, data):
        context['results'] = Reactor.nlp.parse('en', data['statement'])

        return context

################################################################################

@Reactor.router.register_route('linguist', r'^enrich$', strategy='login')
class Enrich(Reactor.ui.Tool):
    form_class    = EnrichForm
    template_name = 'linguist/views/enrich.html'

    def process(self, request, context, data):
        context['results'] = Reactor.nlp.parse('en', data['statement'])

        context['grammars'] = [
            dict(name='minimal', corpus='brown', rules=[
                'NN: {<NN><NN>}',
                'NP: {<DT>?<JJ>*<NN>}',
                'CHUNK: {<V.*> <TO> <V.*>}',
            ]),
            dict(name='default', corpus='brown', rules=[
                'NP:'
                '{<.*>+}          # Chunk everything'
                '}<VBD|IN>+{      # Chink sequences of VBD and IN'
            ]),
        ]

        context['results']['entities'] = [
            (entity,role)
            for entry       in context['results']['analyzed']
            for entity,role in entry['pos_tag']
            if role in (
                'NN','NNP','NNS','NNPS',
                'VB','VBD','VBN',
            )
        ]
        context['results']['topics'] = context['results']['entities']

        context['results']['matches'] = []

        for grammar in context['grammars']:
            grammar['book'] = '\n'.join(grammar['rules'])

            match = dict(
                alias   = grammar['name'],
                grammar = grammar['book'],
                corpus  = getattr(nltk.corpus, grammar['corpus'], nltk.corpus.brown),
                parser  = nltk.RegexpParser(grammar['book']),
            )

            match['result']   = [
                Reactor.nlp.flatten(item)
                for item in match['parser'].parse(context['results']['entities']).pos()
            ]
            match['recurse']  = match['parser'].parse(context['results']['topics']).pos()

            context['results']['matches'].append(match)

            context['results']['topics'] = match['recurse']

        context['results']['topics']   = [
            Reactor.nlp.flatten(item)
            for item in context['results']['topics']
        ]

        return context

#*******************************************************************************

@Reactor.router.register_route('linguist', r'^express$', strategy='login')
class Express(Reactor.ui.Tool):
    form_class    = ExpressForm
    template_name = 'linguist/views/express.html'

    def process(self, request, context, data):
        Reactor.rdf.NS = '''PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX quepy: <http://www.machinalis.com/quepy#>
PREFIX dbpedia: <http://dbpedia.org/ontology/>
PREFIX dbpprop: <http://dbpedia.org/property/>
PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>

'''

        context['results'] = {}

        datatype,mimetype = 'rdf','application/rdf+xml'
        datatype,mimetype = 'turtle','text/turtle'

        from rdflib.namespace import XMLNS,RDF,RDFS,XSD,OWL,SKOS,DOAP,FOAF,DC,DCTERMS,VOID

        raw = Reactor.sparql.mime('http://dbpedia.org/sparql', mimetype, data['statement'])

        grp = rdflib.Graph()
        grp.parse(format=datatype, data=raw)

        for row in grp.query(Reactor.rdf.NS+"""SELECT DISTINCT ?person ?name WHERE {
    ?person foaf:name ?name .
}"""):
            context['results']['people'].append(row)

        for row in grp.query(Reactor.rdf.NS+"""SELECT DISTINCT ?org ?title WHERE {
    ?org foaf:name ?title .
}"""):
            context['results']['organizations'].append(row)

        return context

