#!/usr/bin/env python

from slackclient import SlackClient

from gestalt.cmd.management import *

from uchikoma.opendata.shortcuts import *

################################################################################

class Command(Mongo_Sync):
    help = 'Synchronize Semantics.'

    def add_options(self, parser):
        pass

    #***************************************************************************

    def prepare(self, *args, **options):
        self.connect('mongodb://mist:optimum@ds039135.mlab.com:39135/vortex')

    def terminate(self, *args, **options):
        pass

    #***************************************************************************

    def loop(self, *args, **options):
        cnt = dict(rdf_ns={})

        try:
            print "*) Updating RDF namespaces :"

            data = requests.get('https://prefix.cc/context.json').json()

            for key in data:
                cnt['rdf_ns'][key] = data[key]
        except:
            print "#) Error while scanning : http://prefix.cc/context.json"

        for alias,target in cnt['rdf_ns']:
            yield self.process_rdf_prefix, [alias,target], options

    #***************************************************************************

    def process_rdf_prefix(self, alias, target, **options):
        ns,st     = RdfNamespace.objects.get_or_create(alias=alias)

        ns.link   = target

        ns.save()

        #if st:
        #    scan_rdf_prefix.delay(alias=ns.alias)

        print "\t-> Adding RDF prefix '%(alias)s' at : %(link)s" % ns.__dict__

        #scan_sparql.delay(alias=ep.alias)

