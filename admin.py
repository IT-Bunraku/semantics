from django.contrib import admin

from .models import *

##################################################################################################

class NamespaceAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'link')
    list_editable = ('link',)

admin.site.register(RdfNamespace, NamespaceAdmin)

#*******************************************************************************

class OntologyAdmin(admin.ModelAdmin):
    list_display  = ('namespace', 'alias', 'link')
    list_filter   = ('namespace__alias',)
    list_editable = ('link',)

admin.site.register(OwlOntology, OntologyAdmin)

##################################################################################################

class EndpointAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'link', 'prefixes')
    list_filter   = ('namespaces__alias',)
    list_editable = ('link',)

admin.site.register(SparqlEndpoint, EndpointAdmin)

#*******************************************************************************

class QueryAdmin(admin.ModelAdmin):
    list_display  = ('endpoint','alias', 'statement', 'prefixes')
    list_filter   = ('endpoint__alias','namespaces__alias',)
    list_editable = ('statement',)

admin.site.register(SparqlQuery, QueryAdmin)

##################################################################################################
