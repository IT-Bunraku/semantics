from .utils import *

################################################################################

@Reactor.graph.register_node('people', element='person')
class Person(Neo4Model.Node):
    name = Neo4Prop.String(nullable=False)
    age = Neo4Prop.Integer()

#*******************************************************************************

@Reactor.graph.register_edge('knows', label='knows')
class Knows(Neo4Model.Relationship):
    created = Neo4Prop.DateTime(default=Neo4Util.current_datetime, nullable=False)

