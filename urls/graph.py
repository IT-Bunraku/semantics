from django.conf.urls import include, url

from gestalt.web.utils import Reactor

#from uchikoma.semantics               import rest
from uchikoma.semantics.views         import graph as Home

################################################################################

urlpatterns = Reactor.router.urlpatterns('graph',
    url(r'^explore/$',                             Home.Explorer.neo_graph),
    url(r'^explore/(?P<narrow>[^/]+)/$',           Home.Explorer.neo_graph),
    url(r'^explore/(?P<narrow>[^/]+)/graph.json$', Home.Explorer.neo_payload),

    #url(r'^sparql$',      Home.sparql_cmd,  name='sparql_cmd'),
    #url(r'^search$',      Home.search_box,  name='search_box'),

    #url(r'^$',            Home.Homepage.as_view(), name='homepage'),
)

