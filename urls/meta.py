from django.conf.urls import include, url

from gestalt.web.utils import Reactor

from uchikoma.semantics.views        import schema as Views
from uchikoma.semantics.views.schema import Ontologies as OntoSpect

from django.views.generic.base import TemplateView

################################################################################

urlpatterns = Reactor.router.urlpatterns('meta',
	url(r'^ontology$', OntoSpect.ontology,        name='ontology'),
	url(r'^classes$', OntoSpect.classes,          name='classes'),
	url(r'^properties$', OntoSpect.properties,    name='properties'),
	url(r'^individuals$', OntoSpect.individuals,  name='individuals'),

	# url(r'^browser$', OntoSpect.testinglocal, name='testinglocal'),
	
	
    url(r'^about$', TemplateView.as_view(template_name='ontoview/pages-site/about.html'), name="about"),

	# url(r'^$',	
	# 	direct_to_template, {
	# 		   'template': 'ontoview/pages-site/startsearch.html' , 
	# 	   },  name='startsearch' ,		
	# ),	
	
	url(r'^$', OntoSpect.startsearch,             name='startsearch'),
)

