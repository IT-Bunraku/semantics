from uchikoma.semantics.utils import *

from uchikoma.semantics.models  import *
from uchikoma.semantics.schemas import *
from uchikoma.semantics.graph   import *

#*******************************************************************************

import nltk

################################################################################

@Reactor.rq.register_task(queue='nlp')
def nlp_setup():
    try:
        import spacy.en
    except:
        addr = 'http://index.spacy.io'
        pkgs = 'en==1.1.0'

        os.shell('sputnik --name spacy --repository-url %s install %s' % (addr,pkgs))

    try:
        nltk.download('book')
    except:
        pass

################################################################################

from . import Linguist

