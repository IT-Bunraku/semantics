from uchikoma.semantics.utils import *

from uchikoma.semantics.models  import *
from uchikoma.semantics.schemas import *
from uchikoma.semantics.graph   import *

#*******************************************************************************

import nltk
import quepy

################################################################################

@Reactor.rq.register_task(queue='nlp')
def nlp_sentence(lang_id, statement):
    resp = {
        'language': lang_id,
        'document': statement,
        'analyzed': [],
    }

    for stce in nltk.sent_tokenize(resp['document']):
        item = {
            'sentence': stce,
        }

        item['words']   = nltk.word_tokenize(item['sentence'])
        item['pos_tag'] = nltk.pos_tag(item['words'])

        resp['analyzed'].append(item)

    return resp

#*******************************************************************************

@Reactor.rq.register_task(queue='nlp')
def nlp_entities(lang_id, statement):
    resp = {
        'language': lang_id,
    }

    resp['dialect']  = spacy.load(resp['language'])

    resp['document'] = resp['dialect'](statement)

    resp['entities'] = [x for x in resp['document']]

    return resp

################################################################################

@Reactor.rq.register_task(queue='nlp')
def nlp_lemmas(statement):
    pass

