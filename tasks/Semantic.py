from uchikoma.semantics.utils import *

from uchikoma.semantics.models  import *
from uchikoma.semantics.schemas import *
from uchikoma.semantics.graph   import *

################################################################################

@Reactor.rq.register_task(queue='rdf')
def scan_rdf_namespaces(ns_id):
    pass

#*******************************************************************************

@Reactor.rq.register_task(queue='rdf')
def scan_rdf_ontologies(ns_id, schema):
    pass

################################################################################

@Reactor.rq.register_task(queue='sparql')
def scan_sparql_endpoint(ep_id):
    pass

#*******************************************************************************

@Reactor.rq.register_task(queue='sparql')
def scan_sparql_ontologies(ep_id, schema):
    pass

